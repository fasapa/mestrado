From FP Require Import Base Atom Permutation Term NominalRec.

(* PQ ESSE CASO NÃO FUNCIONOU! *)
(* (**  *α equivalence *) *)
(* Reserved Notation "a ≡α b" (at level 68). *)
(* (* Type not Prop (recursion principle) *) *)
(* Inductive aeq: Λ -> Λ -> Prop := *)
(* | Aeq__var: forall x: Atom.t, 'x ≡α 'x *)
(* | Aeq__app: forall m n m' n': Λ, m ≡α m' -> n ≡α n' -> (m × n) ≡α (m' × n') *)
(* | Aeq__abs: forall a b m n, *)
(*     (forall c, c #('a, 'b, m, n) -> [(a,c)] ∙ₐ m ≡α [(b,c)] ∙ₐ n) -> \a ⋅ m ≡α \b ⋅ n *)
(* where "a ≡α b" := (aeq a b). *)
(* Hint Constructors aeq: core. *)

(* Compara problema quantificação cofinita, encontrar contraexemplo. *)

(**  *α equivalence *)
Reserved Notation "a ≡α b" (at level 68).
Inductive aeq: Λ -> Λ -> Prop :=
| AeqVar: forall x: Atom.t, 'x ≡α 'x
| AeqApp: forall m n m' n': Λ, m ≡α m' -> n ≡α n' -> (m × n) ≡α (m' × n')
| AeqAbs: forall L a b m n,
    (forall c, c ∉ L -> (a ∙ c)ₜ m ≡α (b ∙ c)ₜ n) -> \a ⋅ m ≡α \b ⋅ n
where "a ≡α b" := (aeq a b).
Hint Constructors aeq: core.

Lemma aeq_equivariant p m n: m ≡α n -> (p ∙ₜ m) ≡α (p ∙ₜ n).
Proof.
  induction 1 as [| | L ? ? ? ? HH IH]; intros; simpls;
    autorewrite with nominal; auto; apply fresh AeqAbs with z; rewrite <- (perm_atom_notin_eq p z);
      auto; do 2 rewrite <- perm_action_distr; apply~ IH.
Qed.
Hint Resolve aeq_equivariant: core.

Instance aeq_refl: Reflexive aeq.
Proof.
  intros t; induction t; try constructor~; apply fresh AeqAbs; intros;
    apply (aeq_equivariant [(_,_)]); auto.
Qed.
Hint Resolve aeq_refl: core.

Instance aeq_sym: Symmetric aeq.
Proof. introv AEQ; induction AEQ; intros; try econstructor; eauto. Qed.

Instance aeq_trans: Transitive aeq.
Proof.
  intros ? ? z H; gen z; induction H; simpl; auto; introv HH; inversions HH.
  - constructor~.
  - apply fresh AeqAbs; eauto.
Qed.

Instance aeq_equiv: Equivalence aeq.
Proof. split; typeclasses eauto. Qed.

(** *Alpha conversion and freshness *)
Lemma aeq_perm_action1_cancel a b c m: b#(m) -> c#(m) -> (c ∙ b)ₜ (a ∙ c)ₜ m ≡α (a ∙ b)ₜ m.
Proof.
  destruct (a == b), (a == c), (b == c); subst; try (intros HB HC; autorewrite with nominal); auto;
    try rewriten (perm_action1_switch b c); auto.
  gen a b c; induction m as [| | t ? IH] using nominal_ind; intros a b ? HB c ? ? HC; simpls.
  - rewrite swap_cancel; fsetdec.
  - constructor; [apply IHm1 | apply IHm2]; fsetdec.
  - apply notin_remove_1 in HB as []; apply notin_remove_1 in HC as []; subst;
      autorewrite with nominal; auto.
    + rewriten (swap_neither a c b); auto; apply fresh AeqAbs;
        rewriten (perm_action1_distr c b a c); rewriten (perm_action1_distr a b c b);
          rewrite (perm_action1_switch c a); apply (IH [(_,_)]); auto; fsetdec.
    + apply fresh AeqAbs; rewriten (perm_action1_distr c b a c);
        rewriten (perm_action1_distr a b c b); apply (IH [(_,_)]); auto; fsetdec.
    + destruct (a == t), (b == t), (c == t); subst; simpls; autorewrite with nominal; auto; try congruence.
      * autorewrite with nominal; apply fresh AeqAbs with z.
        rewriten (perm_action1_distr b z t b); rewriten (perm_action1_distr b z c b);
          rewriten (perm_action1_distr b z t c); apply (IH [(_,_)]); auto; fsetdec.
      * rewriten (swap_neither a c t); auto; apply fresh AeqAbs with z;
          rewriten (perm_action1_distr c t a c); rewriten (perm_action1_distr a t c t);
            rewrite (perm_action1_switch c a); apply (IH [(_,_)]); auto; fsetdec.
      * autorewrite with nominal; apply fresh AeqAbs with z.
        rewriten (perm_action1_distr t b a t); rewriten (perm_action1_distr a b t b);
          apply (IH [(_,_)]); auto; fsetdec.
      * autorewrite with nominal; apply fresh AeqAbs with z;
          rewriten (perm_action1_distr t z c b); rewriten (perm_action1_distr t z a c);
            rewriten (perm_action1_distr t z a b); apply (IH [(_,_)]); auto; fsetdec.
Qed.
Hint Resolve aeq_perm_action1_cancel: core.

Lemma aeq_perm_action1_neither a b m: a#(m) -> b#(m) -> (a ∙ b)ₜ m ≡α m.
Proof.
  gen a b; induction m using nominal_ind; simpl.
  - intros; autorewrite with nominal; auto.
  - intros; constructor; auto.
  - intros c z Hc Hz; destruct (c == z); subst; autorewrite with nominal; auto.
    + apply notin_remove_1 in Hc as []; apply notin_remove_1 in Hz as []; subst; autorewrite with nominal; auto.
      * apply fresh AeqAbs;
          do 2 rewriten (perm_action1_distr); apply (H [(_,_)]); auto.
      * apply fresh AeqAbs with w;
          do 2 rewriten perm_action1_distr; rewrite (perm_action1_switch w z); apply (H [(_,_)]); auto.
      * destruct (c == a), (z == a); subst; try congruence; autorewrite with nominal; apply fresh AeqAbs; auto.
        -- rewrite (perm_action1_switch c a); auto.
        -- rewriten perm_action1_distr; apply (H [(_,_)]); auto.
Qed.
Hint Resolve aeq_perm_action1_neither: core.

Lemma aeq_abs_perm b a m: b#(m) -> \a⋅m ≡α \b⋅ ((a ∙ b)ₜ m).
Proof.
  intros; simpls; destruct (a == b); subst; autorewrite with nominal; auto;
    apply fresh AeqAbs; symmetry; auto.
Qed.
Hint Resolve aeq_abs_perm: core.

(** *α structural iteration *)
Definition Pα (P: Λ -> Type) := forall m n, m ≡α n -> P m -> P n.

Lemma abs_aeq_rename:
  forall P: Λ -> Type, Pα P -> forall L,
      (forall b m, b ∉ L -> (forall p, P (p ∙ₜ m)) -> P (\b ⋅ m)) ->
      forall a m, (forall p, P (p ∙ₜ m)) -> P (\a ⋅ m).
Proof.
  intros ? Pa L HL a M H; pick fresh z for (support M ∪ L); apply Pa with (\z ⋅ (a ∙ z)ₜ M).
  - apply aeq_sym; abstract auto.
  - apply HL.
    + abstract auto.
    + intros; pose proof (perm_action_concat p [(a,z)]) as C; simpl in *; rewrite <- C; apply H.
Defined.

(* BVC *)
Definition term_aeq_rect:
  forall P: Λ -> Type,
    Pα P ->
    (forall a, P ('a)) ->
    (forall m n, P m -> P n -> P (m × n)) ->
    {L & forall m a, a ∉ L -> P m -> P (\a ⋅ m)} ->
    forall m, P m.
Proof.
  introv HPa Hv Happ [L HL]; apply nominal_rect; [apply Hv | apply Happ | apply abs_aeq_rename with L];
    auto; introv Hbl HP; apply HL; [| apply (HP [])]; auto.
Defined.

Definition term_aeq_rec := term_aeq_rect (fun _ => Set).

(* BVC Iterator *)
Definition LIt {A: Type} (hv: atom -> A) (hp: A -> A -> A) (fabs: atom -> A -> A) (L: ASet) (l: Λ): A :=
  term_aeq_rect (fun _ => A) (fun _ _ _ => id) hv (fun _ _ => hp) (existT _ L (fun _ b _ r => fabs b r)) l.

(* Iterator properties *)
Lemma LIt_var {A} a fvar fapp fabs L: @LIt A fvar fapp fabs L ('a) = fvar a.
Proof. lazy. auto. Defined.

Lemma LIt_app {A} a b fvar fapp fabs L:
  @LIt A fvar fapp fabs L (a × b) = fapp (@LIt A fvar fapp fabs L a) (@LIt A fvar fapp fabs L b).
Proof. lazy; auto. Defined.

(* term_aeq_rect fully unfolded (excluding names below) *)
Definition term_aeq_aux A (fvar: atom -> A) (fapp: A -> A -> A) (fabs: atom -> A -> A) L M p :=
  term_rect (fun _ => forall p, A) (var_nominal_rec _ fvar) (app_nominal_rec _ (fun _ _ => fapp))
            (abs_nominal_rec _ (abs_aeq_rename (fun _ => A) (fun _ _ _ => id) L (fun b _ _ f => fabs b (f []))))
            M p.

Definition term_aeq_aux_perm_eq A fvar fapp L fabs M :=
  forall p: Π, term_aeq_aux A fvar fapp fabs L M p = term_aeq_aux A fvar fapp fabs L (p ∙ₜ M) [].

Lemma LIt_abs_aux A (fvar: atom -> A) (fapp: A -> A -> A) (fabs: atom -> A -> A) L:
  forall M p, term_aeq_aux A fvar fapp fabs L M p = term_aeq_aux A fvar fapp fabs L (p ∙ₜ M) [].
Proof.
  intros; gen p; induction M as [| ? ? IH1 IH2 |] using nominal_ind; intros.
  - autorewrite with nominal; lazy [term_aeq_aux var_nominal_rec eq_rect_r eq_rect]; simpl;
      case (eq_sym (perm_action_var p t)). (* automatizar esse paço? *) auto.
  - autorewrite with nominal; lazy [term_aeq_aux app_nominal_rec eq_rect_r eq_rect] in *; simpl in *;
      case (eq_sym (perm_action_app p M1 M2)); f_equal; auto.
  - autorewrite with nominal; lazy [term_aeq_aux abs_nominal_rec abs_aeq_rename eq_rect_r eq_rect]; simpl;
      case (eq_sym (perm_action_abs p a M)); destruct (fresh _) as [z ?]; f_equal.
    assert (T1: term_aeq_aux A fvar fapp fabs L M ((p ∙ₐ a, z) :: p) = term_aeq_aux A fvar fapp fabs L (((p ∙ₐ a, z) :: p) ∙ₜ M) []). {
      specialize (H [] ((p ∙ₐ a, z) :: p)); simpl in *; auto. }
    assert (T3: term_aeq_aux A fvar fapp fabs L (((p ∙ₐ a, z) :: p) ∙ₜ M) [] = term_aeq_aux A fvar fapp fabs L (p ∙ₜ M) [(p ∙ₐ a, z)]). {
      specialize (H p [(p ∙ₐ a, z)]); simpl in *; symmetry; auto. }
    eapply eq_trans; eauto.
Qed.

Lemma LIt_abs {A} a M (fvar: atom -> A) (fapp: A -> A -> A) (fabs: atom -> A -> A) L:
  @LIt A fvar fapp fabs L (\a ⋅ M) =
  let (z,_) := fresh (support M ∪ L) in
  fabs z (@LIt A fvar fapp fabs L ([(a,z)] ∙ₜ M)).
Proof.
  destruct (fresh _) as [x ?] eqn: DF; (* pose proof (LIt_abs_aux A fvar fapp fabs L M [(a, x)]); *)
    lazy [LIt term_aeq_rect nominal_rect abs_nominal_rec abs_aeq_rename eq_rect_r eq_rect id]; simpl;
      destruct (fresh _); inversion DF; f_equal; apply (LIt_abs_aux _ _ _ _ _ _ [(a,x)]).
Qed.

(* Definition PSα {A} (f: Λ -> A) m := forall n, m ≡α n -> f m = f n. *)

(* (* Goal forall a b m n, fresh (support (\a ⋅ m)) = fresh (support (\b ⋅ n)). *) *)

(* Lemma LIt_PSα A fvar fapp fabs L M: PSα (@LIt A fvar fapp fabs L) M. *)
(* Proof. *)
(*    induction M using nominal_ind. *)
(*   - unfold PSα; introv Hv; inversion~ Hv. *)
(*   - unfold PSα in *; introv Happ; inversions Happ; *)
(*       do 2 rewrite LIt_app; f_equal; auto. *)
(*   - unfold PSα in *. introv Habs. inversions Habs. *)
(*     do 2 rewrite LIt_abs. do 2 destruct (fresh _). *)
(*     simpl. f_equal. admit. assert (LL: x = x0). admit. *)
(*     subst. specialize (H [(a, x0)]); simpl in *. apply H. *)
(*     pose proof aeq_perm_action1_cancel as A1. pick fresh z. *)
(*     assert (FF1: x0#(M)). fsetdec. assert (FF2: z#(M)). fsetdec. *)
(*     assert (FF3: x0#(n0)). fsetdec. assert (FF4: z#(n0)). fsetdec. *)
(*     assert (FF5: z ∉ L0). fsetdec. *)
(*     lets A2:A1. *)
(*     specialize (A1 a x0 z M FF1 FF2); symmetry in A1. *)
(*     specialize (A2 b x0 z n0 FF3 FF4). specialize (H3 z FF5). *)
(*     pose proof aeq_equivariant as A3. *)
(*     specialize (A3 [(z, x0)] (a ∙ z)ₜM (b ∙ z)ₜn0 H3). simpl in *. *)
(*     pose proof aeq_trans as T1. unfold Transitive in *. *)
(*     lets T2:T1. *)
(*     specialize (T1 _ _ _ A1 A3). *)
(*     specialize (T2 _ _ _ T1 A2). apply T2. *)

(*     assert (FF: z ∉ L0). fsetdec. *)
(*     specialize (H3 z FF).  *)
(*     specialize (A2 ([(z,x0)]) (a ∙ x0)ₜM (b ∙ x0)ₜn0); simpl in *. *)

(*     f_equal. admit. apply H. simpl. apply H3. *)

