Require Import Atom.AtomTactics.

Tactic Notation "apply" "fresh" constr(H) :=
  let z := fresh "F" in
  let L := gather_atoms in (apply H with L); intros z ?.

Tactic Notation "apply" "fresh" constr(H) "with" ident(z) :=
  let L := gather_atoms in (apply H with L); intros z ?.

Tactic Notation "rewriten" "<-" constr(H) := rewrite <- H; autorewrite with nominal.
Tactic Notation "rewriten" constr(H) := rewrite H; autorewrite with nominal.
