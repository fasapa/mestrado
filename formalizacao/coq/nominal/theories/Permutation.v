(** *Swap based permutation *)
Require Import Base Atom.

(* Swap function *)
Definition swap (a b: atom): atom -> atom :=
  fun c => if a == c then b else if b == c then a else c.

Notation "( a ∙ b )ₐ c" := (swap (a%atom) (b%atom) (c%atom))
                            (at level 0, right associativity,
                             format "( a ∙ b )ₐ  c").

(*     (at level 2, a closed binder, b closed binder, right associativity, format "S( a  b )  c"). *)
(* (* Porque level 0? Qualquer valor > 2 causa erro com parentesis, exemplo: destruct (a == b). *) *)

(* Swap properties *)
Lemma swap_id a b: (a ∙ a)ₐ b = b.
Proof. unfold swap; simplify. Qed.
Hint Rewrite -> swap_id: nominal.

Lemma swap_involutive a b c: (a ∙ b)ₐ (a ∙ b)ₐ c = c.
Proof. unfold swap; simplify. Qed.
Hint Rewrite -> swap_involutive: nominal.

Lemma swap_left a b: (a ∙ b)ₐ a = b.
Proof. unfold swap; simplify. Qed.
Hint Rewrite -> swap_left: nominal.

Lemma swap_right a b: (a ∙ b)ₐ b = a.
Proof. unfold swap; simplify. Qed.
Hint Rewrite -> swap_right: nominal.

Lemma swap_neither a b c: c <> a -> c <> b -> (a ∙ b)ₐ c = c.
Proof. unfold swap; simplify. Qed.
Hint Rewrite swap_neither using (solve [congruence | fsetdec]): nominal.

Lemma swap_switch a b c: (a ∙ b)ₐ c = (b ∙ a)ₐ c.
Proof. unfold swap; simplify. Qed.

Lemma swap_distr a b c d e: (a ∙ b)ₐ (c ∙ d)ₐ e = ( (a ∙ b)ₐ c ∙ (a ∙ b)ₐ d )ₐ (a ∙ b)ₐ e.
Proof. unfold swap; simplify. Qed.

Lemma swap_cancel a b c d: b <> d -> c <> d -> (c ∙ b)ₐ (a ∙ c)ₐ d = (a ∙ b)ₐ d.
Proof. unfold swap; simplify. Qed.
Hint Rewrite swap_cancel using (solve [congruence | fsetdec]): nominal.

Lemma swap_neq a b c d: a <> d -> b <> d -> c <> d -> (a ∙ b)ₐ c <> d.
Proof. intros; unfold swap; simplify. Qed.
Hint Resolve swap_neq: core.

Lemma swap_neq1 a b c: a <> b -> b <> c -> (a ∙ b)ₐ c <> a.
Proof. destruct (a == c); subst; simplify; autorewrite with nominal; auto. Qed.
Hint Resolve swap_neq1: core.

Lemma swap_neq2 a b c: a <> b -> b <> c -> (a ∙ b)ₐ c <> a.
Proof. intros; unfold swap; simplify. Qed.
Hint Resolve swap_neq2: core.

Lemma swap_notin_singleton a b c d: a <> b -> a <> c -> a <> d -> a ∉ {{(b ∙ c)ₐ d}}.
Proof. intros; unfold swap; simplify. Qed.
Hint Resolve swap_notin_singleton: core.

Lemma swap_notin_singleton1 a b c: a <> b -> b <> c -> a ∉ {{(a ∙ b)ₐ c}}.
Proof. intros; unfold swap; simplify. Qed.
Hint Resolve swap_notin_singleton1: core.

(** *Swap is a permutation. *)
(* swap a b is a permutation form atom to atom and Bijective thus a permutation. *)
Lemma swap_injective a b: forall x y, (a ∙ b)ₐ x = (a ∙ b)ₐ y -> x = y.
Proof. repeat intro; unfolds swap; simplify; congruence. Qed.
Hint Immediate swap_injective: core.

Lemma swap_surjective a b: forall y, exists x, (a ∙ b)ₐ x = y.
Proof. intro c; exists ((a ∙ b)ₐ c); apply swap_involutive. Qed.

(* Lemma swap_bijective forall a b, Bijective (swap a b). *)
(* Proof. split; [apply swap_injective | apply swap_surjective]. Qed. *)

(** Swap is a finite permutation. Composition of swap is a finite permutation.
    Theorem 1.15 NominalSets. *)

Global Opaque swap.                    (* Prevent simplification from unfolding *)

(** *Perm (swap composition) *)
Notation Π := (list (atom * atom)).

Definition perm_atom (prm: Π): atom -> atom :=
  fun atm => fold_right (fun p a => ((fst p) ∙ (snd p))ₐ a) atm prm.

Notation " p ∙ₐ a " :=
  (perm_atom p (a%atom)) (at level 60, right associativity, format "p  ∙ₐ  a").

Definition perm_atom_inv (p: Π): atom -> atom :=
  fun a => (rev p) ∙ₐ a.

Notation " p ∙ₐ⁻ a " :=
  (perm_atom_inv p (a%atom)) (at level 60, right associativity, format "p  ∙ₐ⁻  a").

Definition atoms (prm: Π): ASet :=
  fold_right (fun (p: atom * atom) a => (fst p) ∪ (snd p) ∪ a) ∅ prm.

Coercion atoms : Π >-> ASet.

Hint Extern 4 (_ ∉ (atoms _)) => simpls; fsetdec: core.

Lemma perm_atom_notin_eq p a: a ∉ atoms p -> p ∙ₐ a = a.
Proof.
  induction p as [| [] ? IH]; intros; simpls~;
    rewrite IH, swap_neither; fsetdec.
Qed.
(* Hint Rewrite -> perm_atom_notin_eq using (try fsetdec): nominal. *)

Lemma perm_atom_notin_neq p a b: a <> b -> a ∉ atoms p -> p ∙ₐ b <> a.
Proof. induction p; intros; simplify; apply swap_neq; fsetdec. Qed.
Hint Resolve perm_atom_notin_neq: core.

Lemma perm_atom_comp_concat p q a: (p ++ q) ∙ₐ a = p ∙ₐ q ∙ₐ a.
Proof. induction p as [| [] ? IH]; intros; simpls~; rewrite~ IH. Qed.

Lemma perm_atom_right_inv: forall p a, p ∙ₐ p ∙ₐ⁻ a = a.
Proof.
  induction p as [| [] ? IH];
    intros; unfolds perm_atom_inv; simpls~; rewrite perm_atom_comp_concat;
      rewrite~ IH; apply swap_involutive.
Qed.
Hint Rewrite -> perm_atom_right_inv: nominal.

(* Lemma perm_left_inv: forall p a, perm_inv p (perm p a) = a. *)
(* Proof. *)
(*   induction p as [| [] ? IH]; intros; unfold perm_inv in *; simpls; auto; *)
(*     rewrite perm_comp_concat; simpls; rewrite swap_involutive; auto. *)
(* Qed. *)
(* Hint Rewrite -> perm_left_inv: nominal. *)

Lemma perm_atom_distr p a b c: p ∙ₐ (a ∙ b)ₐ c = ((p ∙ₐ a) ∙ (p ∙ₐ b))ₐ (p ∙ₐ c).
Proof. induction p as [| ? ? IH]; intros; simpls~; rewrite IH, swap_distr; auto. Qed.

Lemma perm_atom_notin_singleton a b p: a ∉ atoms p -> a <> b -> a ∉ {{p ∙ₐ b}}.
Proof. intros; apply~ notin_singleton_2. Qed.
Hint Resolve perm_atom_notin_singleton: core.

(** *Atom perm is a permutation *)
Lemma perm_atom_injective p: forall x y, p ∙ₐ x = p ∙ₐ y -> x = y.
Proof. induction p as [| ? ? IH]; intros ? ? H; simpls~; apply swap_injective in H; auto. Qed.
Hint Immediate perm_atom_injective: core.

Lemma perm_atom_surjective p: forall y, exists x, p ∙ₐ x = y.
Proof. repeat intro; induction p; intros; eexists; [simpls~ | eapply perm_atom_right_inv]. Qed.

(* Lemma perm_bijective: forall p, Bijective (perm p). *)
(* Proof. split; [apply perm_injective | apply perm_surjective]. Qed. *)

(* (** Swap is a finite permutation. Composition of swap is a finite permutation. *)
(*     Theorem 1.15 NominalSets. *) *)
