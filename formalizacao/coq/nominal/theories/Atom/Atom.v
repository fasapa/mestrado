Require Import Base.
Require Export AtomImpl AtomSetImpl AtomTactics.
Require Export Tactics.

(* Coercion AtomSet.singleton : Atom.t >-> AtomSet.t. *)

(* Ltac notin_union := *)
(*   repeat (match goal with *)
(*           | H: (_ ∉ (_ ∪ _)) |- _ => *)
(*             let U := fresh in *)
(*             lets U:H; apply notin_union_1 in H; apply notin_union_2 in U *)
(*           | H: _ ∉ ⦃ _ ⦄ |- _ => apply notin_singleton_1 in H *)
(*           (* | H: _ ∉ _ |- _ => apply notin_singleton_1 in H *) *)
(*           | _ => idtac *)
(*           end). *)

(* Ltac in_inter := *)
(*   repeat (match goal with *)
(*           | H: _ ∈ (_ ∩ _) |- _ => *)
(*             let U := fresh in *)
(*             lets U:H; apply inter_1 in H; apply inter_2 in U *)
(*           | H: _ ∈ ⦃ _ ⦄ |- _ => apply singleton_1 in H *)
(*           | _ => idtac *)
(*           end). *)
