(** *λ calculus nominal *)
From FP Require Import Base Atom Permutation.

(** Lambda calculus terms *)
Inductive term: Set :=
| Var: atom -> term
| App: term -> term -> term
| Abs: atom -> term -> term.
Hint Constructors term: core.

Declare Scope term_scope.
Delimit Scope term_scope with term.
Bind Scope term_scope with term.

Coercion Var : atom >-> term.
Notation " ' x " := (Var (x%atom)) (at level 64, format "'[' ' x ']'").
Notation " x × y " := (App (x%term) (y%term))
                        (at level 65, right associativity, format "'[' x  ×  y ']'").
Notation " \ x ⋅ y " := (Abs (x%atom) (y%term))
                            (at level 65, right associativity, format "'[' \ x  ⋅  y ']'").

(* "Raw" nominal λ-term. *)
Notation Λ := term.

(* Terms properties *)
Lemma var_inj: forall a b, 'a = 'b <-> a = b.
Proof. split; introv H; inversion~ H. Qed.
Hint Immediate var_inj: core.

Lemma app_inj: forall m n p q, m × n = p × q <-> m = p /\ n = q.
Proof. split; introv H; inversion~ H; subst~. Qed.
Hint Immediate app_inj: core.

Lemma abs_inj: forall a b m n, \a ⋅ m = \b ⋅ n <-> a = b /\ m = n.
Proof. split; introv H; inversion~ H; subst~. Qed.
Hint Immediate abs_inj: core.

(** *Term functions *)
Fixpoint fv (t: Λ): ASet :=
  match t with
  | 'x => x
  | m × n => fv m ∪ fv n
  | \a ⋅ m => a \ fv m
  end.

(* λ terms support *)
Definition support := fv.
Notation " a #( x ) " := (a ∉ support x) (at level 65, format "a #( x )").
Notation " a #( x , .. , y , z ) " := (a ∉ ((support x) ∪ .. ((support y) ∪ (support z)) ..))
                                        (at level 65, format "a #( x ,  .. ,  y ,  z )").

Hint Extern 4 (_ #(\_ ⋅ _)) => simpls; apply~ notin_remove_2: core.
Hint Extern 4 (_ #(\_ ⋅ _)) => simpls; apply~ notin_remove_1: core.

(* Support properties *)
Lemma abs_fresh_eq: forall a m, a#(\a⋅m).
Proof. intros; simpls~. Qed.

Lemma abs_fresh_neq: forall a b m, a#(m) -> a#(\b⋅m).
Proof. intros; simpls~. Qed.

(* Permutation action over λ-term. *)
Fixpoint action a b t: Λ :=
  match t with
  | 'x => (a ∙ b)ₐ x
  | m × n => action a b m × action a b n
  | \x ⋅ m => \(a ∙ b)ₐ x ⋅ action a b m
  end.

Notation " ( a ∙ b )ₜ t " := (action (a%atom) (b%atom) (t%term))
                               (at level 0, right associativity, format "( a  ∙  b )ₜ t").

(* Action properties *)
Lemma action_id a t: (a ∙ a)ₜ t = t.
Proof.
  induction t as [| ? IH1 ? IH2 | ? ? IH]; simpls; autorewrite with nominal;
    [| rewrite IH1, IH2 | rewrite IH]; auto.
Qed.
Hint Rewrite action_id: nominal.

Definition perm_action (prm: Π) (trm: Λ): Λ :=
  fold_right (fun a t => (fst a ∙ snd a)ₜ t) trm prm.

Notation "p ∙ₜ t" := (perm_action p (t%term))
                       (at level 60, right associativity, format "p  ∙ₜ  t").

(* Action properties. *)
Lemma action1 a b t: [(a,b)] ∙ₜ t = (a ∙ b)ₜ t.
Proof. auto. Qed.
Hint Rewrite -> action1: nominal.

Lemma perm_action_var p a: p ∙ₜ ('a) = '(p ∙ₐ a).
Proof. induction p as [| [] ? IH]; simpl; [| rewrite IH]; auto. Defined.
Hint Rewrite -> perm_action_var: nominal.

Lemma perm_action_app p m n: p ∙ₜ (m × n) = (p ∙ₜ m) × (p ∙ₜ n).
Proof. induction p as [| [] ? IH]; simpls; [| rewrite IH]; auto. Defined.
Hint Rewrite -> perm_action_app: nominal.

Lemma perm_action_abs p a t: p ∙ₜ (\a⋅t) = \(p ∙ₐ a) ⋅ (p ∙ₜ t).
Proof. induction p as [| [] ? IH]; simpls; [| rewrite IH]; auto. Defined.
Hint Rewrite -> perm_action_abs: nominal.

Lemma perm_action_id a t: [(a,a)] ∙ₜ t = t.
Proof. autorewrite with nominal; auto. Qed.
Hint Rewrite -> action_id: nominal.

Lemma perm_action_empty: forall t, [] ∙ₜ t = t.
Proof. auto. Defined.

Lemma perm_action_concat p q t: (p ++ q) ∙ₜ t = p ∙ₜ (q ∙ₜ t).
Proof. induction p as [| [] ? IH]; intros; simpls~; rewrite~ IH. Defined.

Lemma perm_action1_injective a b m n: (a ∙ b)ₜ m = (a ∙ b)ₜ n -> m = n.
Proof.
  gen n; induction m as [| ? IH1 ? IH2 |]; intros ? H; destruct n; simpls; try congruence.
  - apply var_inj in H; apply var_inj; eapply swap_injective; eauto.
  - apply app_inj in H as []; erewrite IH1, IH2; eauto.
  - apply abs_inj in H as [A B]; apply swap_injective in A; subst; erewrite IHm; eauto.
Qed.
Hint Immediate perm_action1_injective: core.

Lemma perm_action_injective p m n: p ∙ₜ m = p ∙ₜ n -> m = n.
Proof.
  induction p as [| [A B] ? IH]; intros H; simpls;
    subst~; apply perm_action1_injective in H; auto.
Qed.
Hint Immediate perm_action_injective: core.

Lemma perm_action1_distr a b c d t: (a ∙ b)ₜ (c ∙ d)ₜ t = ((a ∙ b)ₐ c ∙ (a ∙ b)ₐ d)ₜ (a ∙ b)ₜ t.
Proof.
  induction t as [| ? IH1 ? IH2 | ? ? IH]; intros; simpls;
    [ rewrite swap_distr | rewrite IH1, IH2 | rewrite swap_distr, IH]; auto.
Qed.

Lemma perm_action_distr p a b t: p ∙ₜ (a ∙ b)ₜ t = (p ∙ₐ a ∙ p ∙ₐ b)ₜ (p ∙ₜ t).
Proof.
  gen a b t; induction p as [| [] ? IH]; intros; simpls;
    [| rewrite IH, perm_action1_distr]; auto.
Qed.

Lemma perm_action1_switch a b m: (a ∙ b)ₜ m = (b ∙ a)ₜ m.
Proof.
  induction m as [| ? IH1 ? IH2 | ? ? IH]; intros; simpls;
    [ rewrite swap_switch | rewrite IH1, IH2 | rewrite swap_switch, IH]; auto.
Qed.

Lemma perm_action1_involutive a b m: (a ∙ b)ₜ (a ∙ b)ₜ m = m.
Proof.
  induction m as [| ? IH1 ? IH2 | ? ? IH]; intros; simpls; autorewrite with nominal;
    [| rewrite IH1, IH2 | rewrite IH]; auto.
Qed.
Hint Rewrite -> perm_action1_involutive: nominal.

Lemma perm_action1_fresh_left a b m: a <> b -> b#(m) -> a#((a ∙ b)ₜ m).
Proof.
  intros ? H; induction m; simpls;
    [apply swap_notin_singleton1 | idtac
     | apply notin_remove_1 in H as []; subst; [autorewrite with nominal| idtac]];
    fsetdec.
Qed.
(* Hint Resolve perm_action1_fresh_left: core. *)

Lemma perm_action1_fresh_right a b m: a <> b -> b#(m) -> a#((b ∙ a)ₜ m).
Proof. intros; rewrite perm_action1_switch; apply perm_action1_fresh_left; auto. Qed.
(* Hint Resolve perm_action1_fresh_right: core. *)

Lemma perm_action_fresh a p m: a ∉ atoms p -> a#(m) -> a#(p ∙ₜ m).
Proof.
  gen p; induction m as [| ? IH1 ? IH2 | ? ? IH]; introv H1 H2; simpls.
  - rewrite perm_action_var; simpl; apply perm_atom_notin_singleton; fsetdec.
  - autorewrite with nominal; simpls; unfold support in *; apply notin_union_3;
      [apply IH1 | apply IH2]; fsetdec.
  - rewrite perm_action_abs; simpls; apply notin_remove_1 in H2 as []; subst; auto.
    + apply notin_remove_3; autorewrite with nominal; rewrite~ perm_atom_notin_eq.
Qed.
(* Hint Resolve .erm_action_fresh: core. *)

Lemma perm_action1_fresh a b c m: a <> b -> a <> c -> a#(m) -> a#((b ∙ c)ₜ m).
Proof. intros; apply (perm_action_fresh _ [(_,_)]); simpls; fsetdec. Qed.
(* Hint Resolve perm_action1_fresh: core. *)

Ltac solve_fresh :=
  match goal with
  | _: _ |- ?a #((?a ∙ ?b)ₜ _) => apply perm_action1_fresh_left
  | _: _ |- ?a #((?b ∙ ?a)ₜ _) => apply perm_action1_fresh_right
  | _: _ |- ?a #((?b ∙ ?c)ₜ _) => apply perm_action1_fresh
  | _: _ |- _ #(_ ∙ₜ _) => apply perm_action_fresh
  end.

(* a #( m ) = a ∉ ( m ) *)
Hint Extern 3 (_ ∉ ( _ )) => simpl; solve_fresh; simpl; try fsetdec: core.

(* Extra *)
Ltac gather_atoms ::=
	let A := gather_atoms_with (fun x: atom => x) in
  let C := gather_atoms_with (fun x: Λ    => support x) in
  let B := gather_atoms_with (fun x: Π    => atoms x) in
  let D := gather_atoms_with (fun x: ASet => x) in
	constr:(A ∪ B ∪ C ∪ D).


(* (* λ-term size reasoning. *) *)
(* Fixpoint size (t: Λ) {struct t} : nat := *)
(*   match t with *)
(*   | 'x => 1 *)
(*   | m × n => 1 + (size m) + (size n) *)
(*   | \a ⋅ m => 1 + (size m) *)
(*   end. *)

(* Lemma size_action: forall p t, size (p ∙ₐ t) = size t. *)
(* Proof. intros; gen p; induction t; intros; simpls~. Qed. *)

(* (** *Induction principle on nominal λ-terms *) *)
(* Lemma term_nominal_size_ind: *)
(*   forall n, forall t, size t <= n -> forall P: Λ -> Type, *)
(*         (forall x, P ('x)) -> *)
(*         (forall m n, P m -> P n -> P (m × n)) -> *)
(*         (forall a m, (forall p, P (p ∙ₐ m)) -> P (\a ⋅ m)) -> *)
(*         P t. *)
(* Proof. *)
(*   induction n as [| ? IHn]. *)
(*   - intros [] ?; simpls~; intros; Omega.omega. *)
(*   - intros [] ? ? ? APP ABS; simpls~. *)
(*     + apply APP; apply IHn; eauto; Omega.omega. *)
(*     + apply ABS; intros; apply IHn; eauto; rewrite size_action; Omega.omega. *)
(* Defined. *)

(* NÃO DA PRA USAR UTILIZANDO DEFINIÇÃO ACIMA DEVIDO MECANIZMO DE REDUÇÃO DO COQ.
   CBV NÃO TERMINA. *)

(* Lemma term_nominal_ind: *)
(*   forall P: Λ -> Prop, *)
(*     (forall t, P ('t)) -> *)
(*     (forall m n, P m -> P n -> P (m × n)) -> *)
(*     (forall a m, (forall p, P (p ∙ₐ m)) -> P (\a ⋅ m)) -> *)
(*     forall t, P t. *)
(* Proof. *)
(*   refine (fun P VAR APP ABS t => term_nominal_size_ind (size t) t _ P VAR APP ABS); auto. *)
(* Defined. *)
